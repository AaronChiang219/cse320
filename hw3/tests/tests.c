#include <criterion/criterion.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "sfmm.h"

/**
 *  HERE ARE OUR TEST CASES NOT ALL SHOULD BE GIVEN STUDENTS
 *  REMINDER MAX ALLOCATIONS MAY NOT EXCEED 4 * 4096 or 16384 or 128KB
 */

Test(sf_memsuite, Malloc_an_Integer, .init = sf_mem_init, .fini = sf_mem_fini) {
  int *x = sf_malloc(sizeof(int));
  *x = 4;
  cr_assert(*x == 4, "Failed to properly sf_malloc space for an integer!");
}

Test(sf_memsuite, Free_block_check_header_footer_values, .init = sf_mem_init, .fini = sf_mem_fini) {
  void *pointer = sf_malloc(sizeof(short));
  sf_free(pointer);
  pointer = (char*)pointer - 8;
  sf_header *sfHeader = (sf_header *) pointer;
  cr_assert(sfHeader->alloc == 0, "Alloc bit in header is not 0!\n");
  sf_footer *sfFooter = (sf_footer *) ((char*)pointer + (sfHeader->block_size << 4) - 8);
  cr_assert(sfFooter->alloc == 0, "Alloc bit in the footer is not 0!\n");
}

Test(sf_memsuite, SplinterSize_Check_char, .init = sf_mem_init, .fini = sf_mem_fini){
  void* x = sf_malloc(32);
  void* y = sf_malloc(32);
  (void)y;

  sf_free(x);

  x = sf_malloc(16);

  sf_header *sfHeader = (sf_header *)((char*)x - 8);
  cr_assert(sfHeader->splinter == 1, "Splinter bit in header is not 1!");
  cr_assert(sfHeader->splinter_size == 16, "Splinter size is not 16");

  sf_footer *sfFooter = (sf_footer *)((char*)sfHeader + (sfHeader->block_size << 4) - 8);
  cr_assert(sfFooter->splinter == 1, "Splinter bit in header is not 1!");
}

Test(sf_memsuite, Check_next_prev_pointers_of_free_block_at_head_of_list, .init = sf_mem_init, .fini = sf_mem_fini) {
  int *x = sf_malloc(4);
  memset(x, 0, 0);
  cr_assert(freelist_head->next == NULL);
  cr_assert(freelist_head->prev == NULL);
}

Test(sf_memsuite, Coalesce_no_coalescing, .init = sf_mem_init, .fini = sf_mem_fini) {
  int *x = sf_malloc(4);
  int *y = sf_malloc(4);
  memset(y, 0, 0);
  sf_free(x);

  //just simply checking there are more than two things in list
  //and that they point to each other
  cr_assert(freelist_head->next != NULL);
  cr_assert(freelist_head->next->prev != NULL);
}

//#
//STUDENT UNIT TESTS SHOULD BE WRITTEN BELOW
//DO NOT DELETE THESE COMMENTS
//#

Test(sf_memsuite, Coalesce_with_coalescing, .init = sf_mem_init, .fini = sf_mem_fini){
  printf("Free with coalescing test:\n");
  int *a = sf_malloc(4);
  int *b = sf_malloc(4);
  int *c = sf_malloc(4);
  int *d = sf_malloc(4);
  int *e = sf_malloc(4);
  *a = 10;
  *b = 12;
  *c = 13;
  *d = 25;
  *e = 15;
  sf_free(c);
  sf_free(d);
  sf_free(b);
  cr_assert(freelist_head == (sf_free_header*)((char*)b-8));
  cr_assert(freelist_head->header.block_size<<4==96);
  cr_assert(*a==10);
  cr_assert(*e==15);
  sf_snapshot(true);
}

Test(sf_memsuite, realloc_smaller, .init = sf_mem_init, .fini = sf_mem_fini){
  printf("Realloc smaller size\n");
  int *a = sf_malloc(4);
  int *b = sf_malloc(24);
  int *c = sf_malloc(4);
  *a = 15;
  *b = 24;
  *c = 10;
  b = sf_realloc(b,8);
  cr_assert(*b==24);
  sf_header* b_head = (sf_header*)((char*)b-8);
  cr_assert(b_head->splinter == 1 && b_head->splinter_size == 16);
  sf_snapshot(true);

}

Test(sf_memsuite, realloc_larger, .init = sf_mem_init, .fini = sf_mem_fini){
  printf("Realloc larger size\n");
  int *a = sf_malloc(4);
  int *b = sf_malloc(14);
  int *c = sf_malloc(4);
  *a = 15;
  *b = 24;
  *c = 10;
  b = sf_realloc(b, 52);
  cr_assert(*b==24);
  //sf_header* b_head = (sf_header*)((char*)b-8);
  sf_varprint(b);
}

Test(sf_memsuite, invalid_ptr, .init = sf_mem_init, .fini = sf_mem_fini){
  printf("test invalid pointer\n");
  int *a = sf_malloc(4);
  *a = 4;
  sf_free(a-10);
  cr_assert(sf_realloc(a-20,1230)==NULL);
  sf_snapshot(true);
}

Test(sf_memsuite, realloc_preverve_mem, .init = sf_mem_init, .fini = sf_mem_fini){
  printf("realloc preservation test\n");
  char* test = sf_malloc(300);
  memcpy(test, "This is some string that I wish to preserve\n",44);
  printf("%s",test);
  int* a = sf_malloc(348);
  *a = 235;
  test = sf_realloc(test, 584);
  printf("%s",test);
  cr_assert(*a==235);
  cr_assert(strcmp(test,"This is some string that I wish to preserve\n")==0);
  sf_varprint(test);
  sf_varprint(a);
  sf_snapshot(true);
}

Test(sf_memsuite, info_test, .init = sf_mem_init, .fini = sf_mem_fini){
  printf("info printout test\n");
  int* a = sf_malloc(348);
  *a = 24;
  info* test;
  test = sf_malloc(100);
  sf_info(test);
  printf("Allocated Blocks:%ld\n Splintered Blocks:%ld\n Padding in bytes:%ld \n Splinters in bytes:%ld \n Peak Memory Utilization:%f",test->allocatedBlocks, test->splinterBlocks, test->padding, test->splintering, test->peakMemoryUtilization);
  sf_free(a);
  sf_info(test);
  printf("Allocated Blocks:%ld\n Splintered Blocks:%ld\n Padding in bytes:%ld \n Splinters in bytes:%ld \n Peak Memory Utilization:%f",test->allocatedBlocks, test->splinterBlocks, test->padding, test->splintering, test->peakMemoryUtilization);
}

Test(sf_memsuite, random_malloc_free, .init = sf_mem_init, .fini = sf_mem_fini){
  printf("RANDOM MALLOC AND FREE TEST\n");
  int *a = sf_malloc(4);
  int *b = sf_malloc(14);
  int *c = sf_malloc(28);
  int *d = sf_malloc(39);
  int *e = sf_malloc(98);
  int *f = sf_malloc(127);
  int *g = sf_malloc(846);
  int *h = sf_malloc(84);
  int *i = sf_malloc(492);
  sf_free(g);
  sf_free(i);
  sf_free(c);
  int *j = sf_malloc(8);
  int *k = sf_malloc(15);
  int *l = sf_malloc(29);
  sf_snapshot(true);
  d = sf_realloc(d,81);
  e = sf_realloc(e,948);
  sf_free(a);
  sf_free(b);
  sf_free(d);
  sf_free(e);
  sf_free(f);
  sf_free(h);
  sf_free(j);
  sf_free(k);
  sf_free(l);
  sf_snapshot(true);
}
