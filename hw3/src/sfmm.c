/**
 * All functions you make for the assignment must be implemented in this file.
 * Do not submit your assignment with a main function in this file.
 * If you submit with a main function in this file, you will get a zero.
 */
#include "sfmm.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

void coalesce(sf_free_header* header);

void addtofree(sf_free_header* head);

void extendheap();
/**
 * You should store the head of your free list in this variable.
 * Doing so will make it accessible via the extern statement in sfmm.h
 * which will allow you to pass the address to sf_snapshot in a different file.
 */

sf_free_header* freelist_head = NULL;


static int allocatedBlocks = 0;
static int splinterBlocks = 0;
static int padding = 0;
static int splintering = 0;
static int coalesces = 0;
static double peakMemoryUtilization = -1;
static int currMem = 0;
static int maxMem = 0;

static int heap_size= 0;

void *sf_malloc(size_t size) {
	/* First we check if freelist is null, and then use sbrk
	* Then we modify that into one huge free block
	* Then we take the 16-size%16 to calculate padding
	* Then we take the free block, get size, then get size with padding
	* See if the remaining block is less than 32, if so then pad accordingly
	* Then we allocate our free block and make the header footer
	* Then we change the block we split to have a header and footer too
	* Then we traverse the linked list and put ourselves in correct order
	* Blam
	*/
	if (size>16384){
		errno = ENOMEM;
		return NULL;
	} else if (size==0){
		errno = EINVAL;
		return NULL;
	}
	//allocate one block of heap at first, then check the free list for a big enough size, then if not big enough allocate more space and merge the blocks and try again
	if (freelist_head==NULL){
		//initializes the header
		if (heap_size == 0){
			heap_size = 4096;
		} else {
			heap_size +=4096;
		}
		freelist_head = sf_sbrk(1);
		freelist_head->header.alloc = 0;
		freelist_head->header.splinter = 0;
		freelist_head->header.block_size = (4096)>>4;
		freelist_head->next = NULL;
		freelist_head->prev = NULL;
		//initializes the footer
		sf_footer* footer = (sf_footer*) ((char*)freelist_head+4088);
		footer->alloc = 0;
		footer->splinter = 0;
		footer->block_size = (4096)>>4;
	}
	//calculate the total size to search the freelist for
	int pad = 16-(size%16);
	pad = pad==16?0:pad;
	int totalsize = pad+size+16; //8 for header and 8 for footer
	//check if the head is of appropriate size
	//if the head is big enough, allocate, split if no splinter and keep track of pointers
	//if can't split, then set next as header if not null, and change pointers
	int smallest = 20000;
	sf_free_header* temp = freelist_head;
	while (temp!=NULL){
		if (temp->header.block_size<<4>=totalsize && temp->header.block_size<<4<=smallest){
			smallest = temp->header.block_size<<4;
		}
		temp = temp->next;
	}
	temp = freelist_head;
	while (temp!=NULL){
		if (temp->header.block_size<<4==smallest){
			currMem += size;
			if (currMem>=maxMem){
				maxMem=currMem;
				peakMemoryUtilization = (double)maxMem/heap_size;
			}
			allocatedBlocks++;
			int splinter = 0;
			if ((temp->header.block_size<<4)-totalsize<32){
				splinter = (temp->header.block_size<<4)-totalsize;
			}
			sf_footer* oldfooter = (sf_footer*)((char*)temp+(temp->header.block_size<<4)-8);
			oldfooter->block_size = ((temp->header.block_size<<4)-(totalsize))>>4;
			//creates footer for allocated block
			sf_footer* footer = (sf_footer*)((char*)temp+totalsize+splinter-8);
			footer->alloc = 1;
			footer->splinter = splinter==0?0:1;
			footer->block_size = (totalsize+splinter)>>4;
			//creates new header for split free block
			if (splinter==0){
				sf_free_header* newheader = (sf_free_header*)((char*)footer+8);
				//also if the new header is past heap end, we ran out of free space
				if ((void*)newheader>=sf_sbrk(0)){
					freelist_head = NULL;
				} else {
					newheader->header.alloc = 0;
					newheader->header.splinter = 0;
					newheader->header.block_size = ((temp->header.block_size<<4)-totalsize)>>4;
					//if temp is the head, update the head to be the new header
					if (temp==freelist_head){
						newheader->next = freelist_head->next;
						freelist_head = newheader;
						freelist_head->prev = NULL;
						if(freelist_head->next!=NULL){
							freelist_head->next->prev = freelist_head;
						}
					} else {
						//otherwise move around pointers to maintain the linked list
						newheader->next = temp->next;
						newheader->prev = temp->prev;
						if (temp->next!=NULL){
							temp->next->prev = newheader;
						}
						if(temp->prev!=NULL){
							temp->prev->next = newheader;
						}
					}
				}
			} else {
				splinterBlocks++;
				//if there is a splinter, the next should be updated
				if (temp==freelist_head){
					freelist_head = freelist_head->next;
					if(freelist_head != NULL && freelist_head->next!=NULL){
						freelist_head->next->prev = NULL;
					}
				} else {
					temp->prev->next = temp->next;
					if(temp->next!=NULL){
						temp->next->prev = temp->prev;
					}
				}
			}
			//rewrites current header with updated information
			temp->header.alloc = 1;
			temp->header.splinter = splinter==0?0:1;
			temp->header.block_size = (totalsize+splinter)>>4;
			temp->header.requested_size = size;
			temp->header.splinter_size = splinter;
			temp->header.padding_size = pad;
			splintering += splinter;
			padding += pad;
			return (void*) ((char*)temp+8);
		} else {
			temp = temp->next;
		}
	}
	//allocate more heap space and merge with free blocks and call sf_malloc again
	sf_free_header* more = (sf_free_header*) sf_sbrk(1);
	if ((void*)more == (void*)-1){
		errno = EINVAL;
		return NULL;
	}
	heap_size+=4096;
	peakMemoryUtilization = (double)maxMem/heap_size;
	more->header.alloc = 0;
	more->header.splinter = 0;
	more->header.block_size = 4096>>4;
	sf_footer* foot = (sf_footer*)((char*)more+(more->header.block_size<<4)-8);
	foot->alloc = 0;
	foot->splinter = 0;
	foot->block_size = more->header.block_size;
	addtofree(more);
	coalesce(more);
	return sf_malloc(size);
}

void *sf_realloc(void *ptr, size_t size) {
	//calculates the head and foot for the given pointer
	sf_header* head = (sf_header*) ((char*)ptr-8);
	//if head is invalid
	if ((void*)head<(void*)((char*)sf_sbrk(0)-heap_size)){
		errno = EINVAL;
		return NULL;
	}
	sf_footer* foot = (sf_footer*) ((char*)head + (head->block_size<<4)-8);
	//if it is not a valid pointer, set errno and return
	if (foot>=(sf_footer*)sf_sbrk(0)){
		errno = EINVAL;
		return NULL;
	}
	if (head->block_size!=foot->block_size){
		errno = EINVAL;
		return NULL;
	}
	//if size is 0, then free pointer and return freed pointer
	if (size == 0){
		sf_free(ptr);
		return ptr;
	}
	//now we handle all of these cases regarding reallocing space
	int currsize = head->block_size<<4;
	int oldsize = head->requested_size;
	currMem += size;
	currMem -= oldsize;
	if (currMem>=maxMem){
		maxMem=currMem;
		peakMemoryUtilization = (double)maxMem/heap_size;
	}
	//calculates how much space the new size would take
	int pad = 16-(size%16);
	pad = pad==16?0:pad;
	int newsize = size+pad+16;
	if (currsize == newsize){
		head->requested_size = size;
		padding -= head->padding_size;
		head->padding_size = pad;
		padding += pad;
		return ptr;
	} else if (currsize>newsize){
		if (currsize-newsize<32){
			//check if next block is coalescable if not, then mark splinter
			sf_free_header* nexthead = (sf_free_header*)((char*)foot+8);
			//is next head a valid memory address?
			if ((char*)nexthead>=(char*)sf_sbrk(0)){
				//if not then mark splinter and return null
				splinterBlocks -= head->splinter;
				splinterBlocks++;
				head->splinter = 1;
				padding-= head->padding_size;
				head->padding_size = pad;
				padding+= pad;
				splintering -= head->splinter_size;
				head->splinter_size = currsize-newsize;
				head->requested_size = size;
				foot->splinter = 1;
				splintering += (currsize-newsize);
				return ptr;
			} else {
				//if it is, then check if next block is coalescable
				if (nexthead->header.alloc == 0){
					splinterBlocks-= head->splinter;
					sf_free_header* nextnext = nexthead->next;
					sf_free_header* nextprev = nexthead->prev;
					sf_footer* nextfoot = (sf_footer*)((char*)foot+(nexthead->header.block_size<<4));
					nextfoot->block_size = ((nexthead->header.block_size<<4)+(currsize-newsize))>>4;
					head->block_size = newsize>>4;
					sf_footer* newfoot = (sf_footer*)((char*)head+(head->block_size<<4)-8);
					newfoot->alloc = 1;
					newfoot->splinter = 0;
					newfoot->block_size = head->block_size;
					sf_free_header* newhead = (sf_free_header*)((char*)newfoot+8);
					newhead->header.block_size = nextfoot->block_size;
					newhead->header.alloc = 0;
					newhead->header.splinter = 0;
					newhead->header.padding_size = 0;
					newhead->next = nextnext;
					newhead->prev = nextprev;
					if (nextnext!=NULL){
						nextnext->prev = newhead;
					}
					if (nextprev != NULL){
						nextprev->next = newhead;
					}
					if (nexthead==freelist_head){
						freelist_head = newhead;
					}
					padding -= head->padding_size;
					head->padding_size = pad;
					padding += pad;
					head->splinter = 0;
					head->splinter_size = 0;
					head->requested_size = size;
					return ptr;
				} else {
					splinterBlocks-= head->splinter;
					splinterBlocks++;
					head->splinter = 1;
					padding -= head->padding_size;
					head->padding_size = pad;
					padding += pad;
					head->splinter_size = currsize-newsize;
					head->requested_size = size;
					foot->splinter = 1;
					splintering += (currsize-newsize);
					return ptr;
				}
			}
		} else {
			//free and merge
			head->block_size = newsize>>4;
			padding -= head->padding_size;
			head->padding_size = pad;
			padding += pad;
			head->requested_size = size;
			splinterBlocks-= head->splinter;
			splintering -= head->splinter_size;
			head->splinter = 0;
			head->splinter_size = 0;
			sf_footer* newfooter = (sf_footer*) ((char*)head + (head->block_size<<4)-8);
			newfooter->alloc = 1;
			newfooter->splinter = 0;
			newfooter->block_size = head->block_size;
			sf_free_header* newhead = (sf_free_header*)((char*)newfooter+8);
			newhead->header.alloc = 0;
			newhead->header.block_size = (currsize-newsize)>>4;
			foot->alloc = 0;
			foot->splinter = 0;
			foot->block_size = newhead->header.block_size;
			addtofree(newhead);
			coalesce(newhead);
			return ptr;
		}
	} else if (currsize<newsize){
		//ACTUALLY FIRST CHECK IF ADJACENT IS LARGE ENOUGH FOR NEW THING
		//THEN GO THROUGH THE FREE LIST TO SEE IF THERE IS A LARGE ENOUGH BLOCK
		//IF BOTH OF THESE FAIL, then do not modify, set errno and return NULL
		int csize = head->block_size<<4;
		//create pointers for footer and header
		sf_free_header* nexthead = (sf_free_header*)((char*)foot+8);
		//test header validity and then add to calc size
		if ((char*)nexthead >= (char*)sf_sbrk(0)){
			csize += 0;
		} else {
			if (nexthead->header.alloc == 0){
				csize += nexthead->header.block_size<<4;
			}
		}
		//split free block if large enough
		if (csize>=newsize){
			sf_footer* nextfoot = (sf_footer*)((char*)foot+(nexthead->header.block_size<<4));
			//deal with pointer shenanigans before adding new free block
			if (nexthead->next!=NULL){
				nexthead->next->prev = nexthead->prev;
			}
			if (nexthead->prev!=NULL){
				nexthead->prev->next = nexthead->next;
			}
			if (nexthead==freelist_head){
				freelist_head = NULL;
			}
			//is it just large enough to fit everything
			if (csize==newsize){
				splinterBlocks-= head->splinter;
				head->block_size = newsize>>4;
				head->requested_size = size;
				nextfoot->alloc = 1;
				nextfoot->splinter = 0;
				nextfoot->block_size = head->block_size;
				return ptr;
			} else {
				//if not, then find out if it will cause a splinter and either mark appropriately or split and add to free
				if (csize-newsize<32){
					splinterBlocks-= head->splinter;
					splinterBlocks++;
					head->splinter = 1;
					splintering -= head->splinter_size;
					head->splinter_size = (csize-newsize);
					head->block_size = csize;
					nextfoot->alloc = 1;
					nextfoot->splinter = 1;
					nextfoot->block_size = head->block_size;
					splintering += (csize-newsize);
					return ptr;
				} else {
					//if no splinter, create new block and add to free list
					//first create new footer, new header, update next footer and add to free list
					splinterBlocks-= head->splinter;
					head->block_size = newsize>>4;
					head->requested_size = size;
					padding -= head->padding_size;
					head->padding_size = pad;
					padding += pad;
					splintering -= head->splinter_size;
					head->splinter =0;
					head->splinter_size = 0;
					sf_footer* newfoot = (sf_footer*)((char*)head+(head->block_size<<4)-8);
					newfoot->alloc = 1;
					newfoot->splinter = 0;
					newfoot->block_size = head->block_size;
					sf_free_header* newhead = (sf_free_header*)((char*)newfoot+8);
					newhead->header.block_size = (csize-newsize)>>4;
					newhead->header.alloc = 0;
					newhead->header.splinter = 0;
					newhead->header.splinter_size = 0;
					newhead->header.padding_size = 0;
					nextfoot->alloc = 0;
					nextfoot->splinter = 0;
					nextfoot->block_size = newhead->header.block_size;
					addtofree(newhead);
					return ptr;
				}
			}
		}
		void* aptr;
		sf_free_header* temp = freelist_head;
		while (temp!=NULL){
			if ((temp->header.block_size<<4)>=newsize){
				aptr= sf_malloc(size);
				memcpy(aptr,ptr,oldsize);
				sf_free(ptr);
				return aptr;
			} else {
				temp = temp->next;
			}
		}
		if (heap_size==4096*4){
			currMem -= size;
			currMem += oldsize;
			if (currMem>=maxMem){
				maxMem=currMem;
				peakMemoryUtilization = (double)maxMem/heap_size;
			}
			errno = ENOMEM;
			return NULL;
		}
		if ((char*)foot+8==(char*)sf_sbrk(0)){
			extendheap();
			return sf_realloc(ptr,size);
		} else {
			aptr = sf_malloc(size);
			if (aptr==(NULL)){
				currMem -= size;
				currMem += oldsize;
				if (currMem>=maxMem){
					maxMem=currMem;
					peakMemoryUtilization = (double)maxMem/heap_size;
				}
				errno = ENOMEM;
				return NULL;
			}
			memcpy(aptr, ptr, oldsize);
			sf_free(ptr);
			return aptr;
		}
	}
	return NULL;
}

void sf_free(void* ptr) {
	if (ptr>=sf_sbrk(0)){
		errno = EINVAL;
		return;
	}
	sf_header* header = (sf_header*)((char*)ptr-8);
	if ((void*)header<(void*)((char*)sf_sbrk(0)-heap_size)){
		errno = EINVAL;
		return;
	}
	int size = header->block_size<<4;
	sf_footer* foot = (sf_footer*)((char*)header+size-8);
	if ((foot>(sf_footer*)sf_sbrk(0))) {
		errno = EINVAL;
		return;
	}
	if (foot->block_size!=header->block_size){
		errno = EINVAL;
		return;
	}
	currMem -= header->requested_size;
	if (currMem>=maxMem){
		maxMem=currMem;
		peakMemoryUtilization = (double)maxMem/heap_size;
	}
	header->alloc = 0;
	foot->alloc = 0;
	splinterBlocks-= header->splinter;
	splintering -= header->splinter_size;
	allocatedBlocks--;
	addtofree((sf_free_header*)header);
	coalesce((sf_free_header*)header);
	return;
}

int sf_info(info* ptr) {
	if (peakMemoryUtilization==-1){
		return -1;
	}
	ptr->allocatedBlocks = allocatedBlocks;
	ptr->splinterBlocks = splinterBlocks;
	ptr->padding = padding;
	ptr->splintering = splintering;
	ptr->peakMemoryUtilization = peakMemoryUtilization;
	return 0;
}

//checks previous free block and next free block and coalesces if possible
void coalesce(sf_free_header* header){
	int size = header->header.block_size<<4;
	int a = 0;
	sf_footer* foot = (sf_footer*)((char*)header+size-8);
	if (foot->block_size!=header->header.block_size){
		errno = EINVAL;
		return;
	}
	sf_free_header* next = (sf_free_header*)((char*)foot+8);
	if (next==header->next){
		a = 1;
		if (header->next->next!=NULL){
			header->next->next->prev = header;
			header->next = next->next;
		} else {
			header->next = next->next;
		}
		int offset = next->header.block_size;
		header->header.block_size = header->header.block_size+next->header.block_size;
		sf_footer* n_foot = (sf_footer*)((char*)foot+(offset<<4));
		n_foot->alloc = 0;
		n_foot->splinter = 0;
		n_foot->block_size = header->header.block_size;
		//update footer
		foot = n_foot;

	}
	sf_footer* p_foot = (sf_footer*)((char*)header-8);
	sf_free_header* prev = (sf_free_header*) ((char*)header-(p_foot->block_size<<4));
	if (prev==header->prev){
		a = 1;
		prev->header.block_size = prev->header.block_size+header->header.block_size;
		prev->next = header->next;
		if (header->next!=NULL){
			header->next->prev = prev;
		}
		header = prev;
		foot->block_size = header->header.block_size;
		foot->alloc = 0;
		foot->splinter = 0;
	}
	foot->block_size = header->header.block_size;
	foot->alloc = 0;
	foot->splinter = 0;
	coalesces += a;
	return;
}

//adds free block to free list
void addtofree(sf_free_header* head){
	sf_free_header* temp = freelist_head;
	sf_free_header* last = NULL;
	if(freelist_head == NULL){
		freelist_head = head;
		freelist_head->next = NULL;
		freelist_head->prev = NULL;
		return;
	}
	while (temp!=NULL){
		if (temp>head){
			head->next = temp;
			head->prev = temp->prev;
			if (temp->prev!=NULL){
				temp->prev->next = head;
				temp->prev = head;
			} else {
				temp->prev = head;
			}
			if (temp==freelist_head){
				freelist_head = head;
			}
			return;
		}
		if (temp->next==NULL){
			last = temp;
		}
		temp = temp->next;
	}
	last->next = head;
	head->prev = last;
	head->next = NULL;
	return;
}

void extendheap(){
	if(heap_size!=4096*4){
		if (freelist_head==NULL){
			//initializes the header
			if (heap_size == 0){
				heap_size = 4096;
			} else {
				heap_size +=4096;
			}
			peakMemoryUtilization = maxMem/heap_size;
			freelist_head = sf_sbrk(1);
			freelist_head->header.alloc = 0;
			freelist_head->header.splinter = 0;
			freelist_head->header.block_size = (4096)>>4;
			freelist_head->next = NULL;
			freelist_head->prev = NULL;
			//initializes the footer
			sf_footer* footer = (sf_footer*) ((char*)freelist_head+4088);
			footer->alloc = 0;
			footer->splinter = 0;
			footer->block_size = (4096)>>4;
		} else {
			sf_free_header* more = (sf_free_header*) sf_sbrk(1);
			heap_size+=4096;
			more->header.alloc = 0;
			more->header.splinter = 0;
			more->header.block_size = 4096>>4;
			sf_footer* foot = (sf_footer*)((char*)more+(more->header.block_size<<4)-8);
			foot->alloc = 0;
			foot->splinter = 0;
			foot->block_size = more->header.block_size;
			addtofree(more);
			coalesce(more);
		}
	}
}
