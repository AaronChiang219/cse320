#ifndef INFO_H
#define INFO_H

#ifdef INFO
	#define info(msg, msg2) fprintf(stderr,"INFO: %s: %s\n", msg, msg2)
	#define infoa(len, blah, len2, blah2) fprintf(stderr, "INFO: shifted alphabet: %.*s" "%.*s\n",len,blah,len2,blah2)
#else
	#define info(msg, msg2)
	#define infoa(len, blah, len2, blah2)
#endif


#endif
