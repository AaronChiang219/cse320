#include "hw1.h"
#include "info.h"
int main(int argc, char **argv) {

    FILE* in;
    FILE* out;
    char args;
    /* Note: create a variable to assign the result of validargs */
    args = validargs(argc, argv, &in, &out);
    if ((args&0x80)==0x80){
    	USAGE(EXIT_SUCCESS);
    } else if((args&0x40)==0x40){
    	subcipher(args, in, out);
        infoa(arraylength(Alphabet)-(args&0x1F),Alphabet+(args&0x1F),(args&0x1F),Alphabet);
        info("shift amount",argc==6?*(argv+5):"320");
        info("input file", **(argv+3)=='-'?"STDIN":*(argv+3));
        info("output file", **(argv+4)=='-'?"STDOUT":*(argv+4));
        info("operation", *(*(argv+2)+1)=='e'?"encryption":"decryption");
    	return EXIT_SUCCESS;
    } else if (args == 0){
        USAGE(EXIT_FAILURE);
    } else if((args&0x40)==0){
        return tutcipher(args, in, out);
    } else {
    	USAGE(EXIT_FAILURE);
    }
}
