#include "hw1.h"

// For your helper functions (you may add additional files also)
// DO NOT define a main function here!

char validargs(int argc, char** argv, FILE** in, FILE** out) {
	char ret = 0;
	if (argc>1){
		if (validflag(*(argv+1))=='h'){
			ret = 0x80;
			return ret;
		}
		if (argc>=5 && (fopen(*(argv+3),"r")!=NULL || validflag(*(argv+3))=='-')){
			*in = validflag(*(argv+3))=='-'?stdin:fopen(*(argv+3),"r");
			*out = validflag(*(argv+4))=='-'?stdout:fopen(*(argv+4),"w+");
			char flag = validflag(*(argv+1));
			char flag_2 = validflag(*(argv+2));
			if ((flag == 's' || flag =='t') && (flag_2 == 'e' || flag_2 == 'd')){
				ret = ret^(flag=='s'?0x40:0);
				ret = ret^(flag_2=='d'?0x20:0);
				unsigned int a = (argc==6?convertint(*(argv+5)):320);
				a %= arraylength(Alphabet);
				ret = ret^a;
				return ret;
			}
			ret = 0;
			return ret;
		}
	}
	ret = 0;
	return ret;
}
void subcipher(char args, FILE* in, FILE* out){
	int shift = args&0x1F;
	char input = '0';
	int alength = arraylength(Alphabet);
	// if we want to decode then decode
	if ((args&0x20)==0x20){
		while((input=fgetc(in))!=EOF){
			int length;
			input += (input>='a' && input <='z')?('A'-'a'):0;
			char* bet = Alphabet;
			for (length = 0;*bet!='\0' && input!=*bet;length++,bet++){
				;
			}
			if (length == alength){
				fputc(input, out);
			} else {
				fputc(*(Alphabet+((length-shift+alength)%alength)),out);
			}
		}
	} else {
		while(((input=fgetc(in))!=EOF)){
			int length;
			char *bet = Alphabet;
			if (input>='a' && input <='z'){
				input += ('A'-'a');
			}
			for (length = 0;*bet!='\0' && input!=*bet;length++,bet++){
				;
			}
			if (length == alength){
				fputc(input, out);
			} else {
				fputc(*(Alphabet+((length+shift)%alength)),out);
			}
		}
	}
	fclose(in);
	fclose(out);
	return;
}
int tutcipher(char args, FILE* in, FILE* out){
	char current;
	//encoding section
	if ((args&0x20)==0){
		if ((current=fgetc(in))!=EOF){
			char next = fgetc(in);
			while (current!=EOF){
				if (current == next || current-('A'-'a')==next || next-('A'-'a')==current){
					if (current>='A' && current<='Z'){
						fputc('S',out);
						current -= ('A'-'a');
					} else {
						fputc('s',out);
					}
					switch(current){
						case 'a': case 'e': case 'i':
						case 'o': case 'u':
						fputs("quat",out);
						fputc(next,out);
						break;
						default:
						fputs("qua",out);
						getsub(next, buffer, Tutnese);
						fputs(buffer, out);
						break;
					}
					current = fgetc(in);
					next = fgetc(in);
				} else {
					getsub(current, buffer, Tutnese);
					fputs(buffer, out);
					current = next;
					next = fgetc(in);
				}
			}
		}
		//decoding section
	} else {
		char input;
		while ((input=fgetc(in))!= EOF){
			int capitalize = (input>='A' && input<='Z')?('A'-'a'):0;
			input -= capitalize;
			int failure;
			switch(input){
				case 's':
				*buffer=fgetc(in);
				*(buffer+1)=fgetc(in);
				*(buffer+2)=fgetc(in);
				if (*buffer=='q' && *(buffer+1)=='u' && *(buffer+2)=='a'){
					input = fgetc(in);
					if (input=='t'){
						input = fgetc(in);
						if (input<='Z' && input>='A'){
							fputc(input-('A'-'a')+capitalize,out);
							fputc(input,out);
							break;
						} else {
							fputc(input+capitalize,out);
							fputc(input,out);
							break;
						}
					} else if (input<='Z' && input>='A'){
						fputc(input-('A'-'a')+capitalize,out);
						fseek(in,-1, SEEK_CUR);
						//break;
					} else {
						fputc(input+capitalize,out);
						fseek(in,-1, SEEK_CUR);
						//break;

					}
				} else {
					if(*(buffer+2)==EOF){
						fseek(in,-1,SEEK_CUR);
					} else {
						fseek(in,-3,SEEK_CUR);
					}
					if (*(buffer+1)==EOF){
						fseek(in,-1,SEEK_CUR);
					}
					if (*buffer==EOF){
						fseek(in,-1,SEEK_CUR);
					}
					if (input>='a' && input <= 'z'){
						fputc(input+capitalize,out);
						failure = advinput(input, in, Tutnese);
						if (failure == -1){
							return EXIT_FAILURE;
						}
					} else {
						fputc(input,out);
					}
				}
				break;
				case 'a': case 'e': case 'i': case 'o': case 'u':
				fputc(input+capitalize, out);
				break;
				default:
				if (input>='a' && input <= 'z'){
					fputc(input+capitalize,out);
					failure = advinput(input, in, Tutnese);
					if (failure==-1){
						return EXIT_FAILURE;
					}
				} else {
					fputc(input,out);
				}
				break;
			}
		}
	}
	return EXIT_SUCCESS;
}
//advances input file when decoding Tutnese
int advinput(char input, FILE* in, char** Tutnese){
	for (;input!=**Tutnese && *Tutnese!=NULL;Tutnese++){;}
		if (*Tutnese==NULL){
			return 0;
		} else {
			int length;
			char* test = *Tutnese;
			for (length = -1;*++*Tutnese!='\0' && **Tutnese==fgetc(in);length--){;}
			if(**Tutnese=='\0'){
				*Tutnese = test;
				return 0;
			} else {
				*Tutnese = test;
				fseek(in,length,SEEK_CUR);
				return -1;
			}
		}
}
//gets substitution in tutnese for a char and stores
//in buffer
void getsub(char tut, char* buffer, char** Tutnese){
	*buffer = tut;
	tut -= (tut>='A' && tut<='Z')?('A'-'a'):0;
	for (; *Tutnese!=NULL && **Tutnese!=tut;Tutnese++){
		;
	}
	if (*Tutnese!=NULL){
		char* test = *Tutnese;
		for (;**Tutnese!='\0';++buffer,*buffer=*(++*Tutnese)){
			;
		}
		*Tutnese = test;
		*++buffer = '\0';
	} else {
		*++buffer = '\0';
	}
	return;
}
char validflag(char* flag){
	if (*flag=='-' && *(flag+1) == '\0'){
		return *flag;
	}
	if (*flag=='-' && *(flag+2) =='\0'){
		return *(flag+1);
	}
	return '\0';
}
int convertint(char* number){
	int value;
	for (value = 0; *number<='9' && *number>='0';value *= 10, value += *number-'0', number++){
		;
	}
	return value;
}
int arraylength(char* Array){
	int length;
	for (length = 0;*Array!='\0';length++, Array++){
		;
	}
	return length;
}
