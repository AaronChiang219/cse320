#include "hw2.h"

int main(int argc, char *argv[]){
    strcpy(DEFAULT_DICT_FILE, "rsrc/dictionary.txt");
    strcpy(NEW_DICT_FILE,"rsrc/new_dictionary.txt");
    DEFAULT_INPUT = stdin;
    DEFAULT_OUTPUT = stdout;
    changedict = false;
    //create dictionary
    if((dict = (struct dictionary*) malloc(sizeof(struct dictionary))) == NULL) {
        printf("ERROR: OUT OF MEMORY.\n");
        return EXIT_FAILURE;
    }
    dict->word_list = NULL;
    m_list = NULL;
    bool validarg = true;
    /*struct Args args;
    // Set struct default values
    args.d = false;
    args.i = false;
    args.o = false;
    strcpy(args.dictFile, DEFAULT_DICT_FILE);*/
    // Make a loop index
    //int i;
    //char line[MAX_SIZE];
    //Declare Files
    FILE* dFile = NULL;
    FILE* iFile = DEFAULT_INPUT;
    FILE* oFile = DEFAULT_OUTPUT;

    int n = 0;
    char opt = '\0';
    bool default_dict = true;
    //write a better version of option parsing
    while ((opt = getopt(argc,argv,"i:o:d:A::h"))!=-1){
    	switch(opt){
    		case 'h':
    		if (iFile!=NULL && iFile!=stdin){
    			fclose(iFile);
    		}
    		if (oFile!=NULL && oFile!=stdout) {
    			fclose(oFile);
    		}
    		if (dFile!=NULL) {
    			fclose(dFile);
    		}
    		free(dict);
    		USAGE(EXIT_SUCCESS);
    		break;
    		case 'i':
    		iFile = fopen(optarg, "r");
    		break;
    		case 'o':
    		oFile = fopen(optarg, "w");
    		break;
    		case 'd':
    		default_dict = false;
    		dFile = fopen(optarg,"r");
    		if (dFile!=NULL){
    			strcpy(DEFAULT_DICT_FILE,optarg);
    			strcpy(NEW_DICT_FILE,dirname(optarg));
    			strcat(NEW_DICT_FILE,"/new_");
    			strcat(NEW_DICT_FILE,basename(DEFAULT_DICT_FILE));
    		}
    		break;
    		case 'A':
    		if (optarg!=NULL && *optarg>='0' && *optarg<='5' && *(optarg+1)=='\0'){
    			n = *optarg-'0';
    		} else {
                fprintf(stderr, "Invalid # of misspellings specified\n");
                if (dFile!=NULL){
                    fclose(dFile);
                }
                if (oFile!=NULL && oFile!=stdout){
                    fclose(oFile);
                }
                if (iFile!=NULL && iFile!=stdin){
                    fclose(iFile);
                }
                free(dict);
                USAGE(EXIT_FAILURE);
            }
            break;
    		default:
    		validarg = false;
    		break;
    	}
    }
    if (!validarg){
    	if (dFile!=NULL){
    	    fclose(dFile);
    	}
    	if (oFile!=NULL && oFile!=stdout){
    	    fclose(oFile);
    	}
    	if (iFile!=NULL && iFile!=stdin){
    	    fclose(iFile);
    	}
    	free(dict);
    	USAGE(EXIT_FAILURE);
    }
    if (default_dict == true){
    	dFile = fopen(DEFAULT_DICT_FILE,"r");
    }
    if (iFile == NULL){
    	fprintf(stderr, "Unable to open input file \n");
    	if (dFile!=NULL){
    		fclose(dFile);
    	}
    	if (oFile!=NULL && oFile!=stdout){
    		fclose(oFile);
    	}
    	USAGE(EXIT_FAILURE);
    }
    if (dFile == NULL){
    	fprintf(stderr,"Unable to open dictionary file \n");
        if (oFile!=NULL && oFile!=stdout){
            fclose(oFile);
        }
        if (iFile!=NULL && iFile!=stdin){
            fclose(iFile);
        }
        USAGE(EXIT_FAILURE);
    } else {
    	if (processDictionary(dFile)==false){
            fclose(dFile);
            if (oFile!=NULL && oFile!=stdout){
                fclose(oFile);
            }
            if (iFile!=NULL && iFile!=stdin){
                fclose(iFile);
            }
            if (dict->word_list!=NULL){
                freeWords(dict->word_list);
            }
            free(dict);
            if (m_list!=NULL){
                freeMisspelledWords(m_list);
            }
            USAGE(EXIT_FAILURE);
        }
    }

    //function that processes input seperately
    char word[MAX_SIZE];
    char* wdPtr = word;
    //char line[MAX_SIZE];
    //char* character = line;
    char inputc = '\0';
    //whelp looks like I have to rewrite this part cause it's trash
    while(!feof(iFile))
    {
        inputc = tolower(fgetc(iFile));
        switch(inputc){
            case ' ': case '\n': case '\t':
            fputc(inputc,oFile);
            break;
            case EOF:
            break;
            default:
            *wdPtr++=inputc;
            while ((*wdPtr=tolower(fgetc(iFile)))!=EOF && *wdPtr!=' ' && *wdPtr!= '\n' && *wdPtr!='\t'){wdPtr++;}
            if (*wdPtr==EOF){
                *wdPtr='\0';
            }
            *++wdPtr= '\0'; //append null char
            //now deal with leading and trailing special characters
            wdPtr-=2;
            while (!(*wdPtr>='a' && *wdPtr<='z')){
                wdPtr--;
            }
            //now wdPtr points at last character without trailing symbols
            if (!processWord(word, wdPtr, oFile,n)){
            	if (iFile!=stdin){
            		fclose(iFile);
            	}
            	if (oFile!=stdout){
            		fclose(oFile);
            	}
            	fclose(dFile);
            	freeWords(dict->word_list);
            	//free dictionary
            	free(dict);
            	//free m_list
            	freeMisspelledWords(m_list);
            	return EXIT_FAILURE;
            }
            wdPtr = word;
            break;
        }
    }
    //instead of this I need to put out a new dictionary file if -An is on
    //then I need to print statistics on mispellings and such
    printWords(dict->word_list);
    if (changedict){
    	newDictionary(NEW_DICT_FILE);
    }
    //printf("\n--------FREED WORDS--------\n");
    freeWords(dict->word_list);
    //free dictionary
    free(dict);
    //free m_list
    freeMisspelledWords(m_list);

    fclose(dFile);
    fclose(iFile);
    fclose(oFile);
    return EXIT_SUCCESS;
}
