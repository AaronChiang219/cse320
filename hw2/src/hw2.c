#include "hw2.h"

/* Great filename. */
bool processDictionary(FILE* f){
    dict->num_words = 0;
	struct dict_word* currWord;
	struct misspelled_word* currMisspelling;
    if((currWord = (struct dict_word*) malloc(sizeof(struct dict_word))) == NULL)
    {
        printf("OUT OF MEMORY.\n");
        return false;
    }
    if((currMisspelling = (struct misspelled_word*) malloc(sizeof(struct misspelled_word))) == NULL)
    {
        printf("ERROR: OUT OF MEMORY.");
        return false;
    }
    m_list = currMisspelling;
    currWord->num_misspellings = 0;
    currWord->misspelled_count = 0;
    dict->word_list = currWord;
    dict->num_words++;
    char word[MAX_SIZE];
    char* wdPtr = word;
    //char line[MAX_SIZE];
	//char* character = line;
	char inputc = '\0';
    int counter = 0;
	int firstWord = 1;
    while(inputc!=EOF)
    {
    	inputc = fgetc(f);
    	switch(inputc){
    		case ' ':
    		break;
    		case '\n':
    		//allocates memory for next word
    		if(((currWord->next) = (struct dict_word*) malloc(sizeof(struct dict_word))) == NULL)
    		{
    		    printf("OUT OF MEMORY.\n");
    		    return false;
    		}
    		while ((*wdPtr=fgetc(f))== ' ' || *wdPtr == '\n' || *wdPtr == '\t'){;}
    		if (*wdPtr==EOF){
    			free(currWord->next);
                freeLast(m_list);
    			currWord->next = NULL;
    			return true;
    		} else {
    			fseek(f,-1,SEEK_CUR);
    		}
    		//changes pointer to point to current word
    		currWord = currWord->next;
    		//resets first word condition
    		firstWord = 1;
    		//resets counter of misspelled words
    		counter = 0;
    		break;
    		case EOF:
    		break;
    		default:
    		if (firstWord==1){
    			*wdPtr++=inputc;
    			while ((*wdPtr=fgetc(f))!=EOF && *wdPtr!=' ' && *wdPtr!='\n' && *wdPtr!='\t'){wdPtr++;}
    			if (*wdPtr=='\n'){
    				fseek(f,-1,SEEK_CUR);
    			}
    			*wdPtr = '\0';
    			firstWord = 0;
    			//add word to dictionary
    			addWord(currWord, word);
    			wdPtr = word;
    		} else if (firstWord==0 && counter<MAX_MISSPELLED_WORDS){
    			*wdPtr++=inputc;
    			while ((*wdPtr=fgetc(f))!=EOF && *wdPtr!=' ' && *wdPtr!='\n' && *wdPtr!='\t'){wdPtr++;}
    			if (*wdPtr=='\n'){
    				fseek(f,-1,SEEK_CUR);
    			}
    			*wdPtr = '\0';
    			//add mispelling to mispelled dictionary
    			addMisspelledWord(currMisspelling,currWord,word);
    			//allocates space for next mispelling
    			if(((currMisspelling->next) = (struct misspelled_word*) malloc(sizeof(struct misspelled_word)))== NULL)
    			{
    			    printf("ERROR: OUT OF MEMORY.");
    			    return false;
    			}
    			//points mispelling to current mispelling
    			currMisspelling = currMisspelling->next;
                currMisspelling->next = NULL;
    			counter++;
    			wdPtr = word;
    		}
    		break;
    	}
    }
    if (m_list->next!=NULL){
        freeLast(m_list);
    }
    return true;
}

void addWord(struct dict_word* dWord, char* word){
    //setting up dWord fields
    dWord->misspelled_count = 0;
    dWord->num_misspellings = 0;
    dict->num_words++;
    strcpy(dWord->word, word);
}

void addMisspelledWord(struct misspelled_word* misspelledWord, struct dict_word* correctWord, char* word){
    //setting up misspelledWord fields
    strcpy(misspelledWord->word, word);
    misspelledWord->misspelled = false;
    misspelledWord->correct_word = correctWord;
    (correctWord->misspelled)[correctWord->num_misspellings] = misspelledWord;
    correctWord->num_misspellings++;
}

void freeLast(struct misspelled_word* currWord){
    while (currWord->next->next!=NULL){
        currWord = currWord->next;
    }
    free(currWord->next);
    (currWord)->next = NULL;
}

void freeWords(struct dict_word* currWord){
    if (currWord->next!=NULL){
    	freeWords(currWord->next);
    }
    free(currWord);
    return;
}

void freeMisspelledWords(struct misspelled_word* currWord){
    if (currWord->next!=NULL){
    	freeMisspelledWords(currWord->next);
    }
    free(currWord);
    return;
}

void printWords(struct dict_word* currWord){
	int sizedict = 0;
	int sizemiss = 0;
	int numspell = 0;
	struct misspelled_word* ptr = m_list;
	fprintf(stderr, "Total number of words in dictionary:%d\n",dict->num_words);
	sizedict+=sizeof(dict);
	while(currWord->next!=NULL){
		if ((currWord->misspelled_count)>0){
			numspell++;
		}
		sizedict+=sizeof(currWord);
		currWord = currWord->next;
	}
	if ((currWord->misspelled_count)>0){
		numspell++;
	}
	sizedict+=sizeof(currWord);
	fprintf(stderr, "Size of dictionary (in bytes):%d\n",sizedict);
	currWord = dict->word_list;
	while (ptr->next!=NULL){
		sizemiss+=sizeof(ptr);
		ptr = ptr->next;
	}
	sizemiss+=sizeof(ptr);
	ptr = m_list;
	fprintf(stderr, "Size of misspelled word list (in bytes):%d\n",sizemiss);
	fprintf(stderr, "Total number of misspelled words:%d\n",numspell);
	fprintf(stderr, "Top 3 Misspelled Words:\n");
	//now somehow sort through the linked list for the top 3 misspelled words
	int max = 0;
	int print = 0;
	while (currWord->next!=NULL){
		max = max<currWord->misspelled_count?currWord->num_misspellings:max;
		currWord = currWord->next;
	}
	max = max<currWord->misspelled_count?currWord->num_misspellings:max;
	while(print!=3){
		currWord = dict->word_list;
		while (currWord->next!=NULL && print!=3){
			if (currWord->misspelled_count==max){
				bool first = true;
				fprintf(stderr,"%s (%d times):",currWord->word,max);
				for (int i = 0; i<currWord->num_misspellings;i++){
					if (currWord->misspelled[i]->misspelled){
						if (first){
							fprintf(stderr, "%s",currWord->misspelled[i]->word);
							first = false;
						} else {
							fprintf(stderr,", %s",currWord->misspelled[i]->word);
						}
					}
				}
				fprintf(stderr,"\n");
				print++;
			}
			currWord = currWord->next;
		}
		if (print==3){
			break;
		}
		if (currWord->misspelled_count==max){
			bool first = true;
			fprintf(stderr,"%s (%d times):",currWord->word,max);
			for (int i = 0; i<currWord->num_misspellings;i++){
				if (currWord->misspelled[i]->misspelled){
					if (first){
						fprintf(stderr, "%s",currWord->misspelled[i]->word);
						first = false;
					} else {
						fprintf(stderr,", %s",currWord->misspelled[i]->word);
					}
				}
			}
			fprintf(stderr,"\n");
			print++;
		}
		max--;
	}

}

bool processWord(char* inputWord, char* endWord, FILE* f, int n){
	char word[MAX_SIZE];
	char* w = word;
	while (!(*inputWord>='a' && *inputWord<='z')){
		fputc(*inputWord,f);
		inputWord++;
	}
	while (inputWord<=endWord){
		*w=*inputWord;
		inputWord++;
		w++;
	}
	*w = '\0';
	if (foundMisspelledMatch(word)){
		fwrite(word,1,strlen(word),f);
		endWord++;
		while (*endWord!='\0'){
			fputc(*endWord,f);
			endWord++;
		}
		return true;
	} else if (foundDictMatch(word)){
		fwrite(word,1,strlen(word),f);
		endWord++;
		while (*endWord!='\0'){
			fputc(*endWord,f);
			endWord++;
		}
		return true;
	} else {
		//put in misspellings adder
		if (n!=0){
			changedict = true;
			struct dict_word* currWord;
			struct dict_word* ptr = dict->word_list;
			if((currWord = (struct dict_word*) malloc(sizeof(struct dict_word))) == NULL)
			{
			    fprintf(stderr,"OUT OF MEMORY.\n");
			    return false;
			}
			addWord(currWord,word);
			currWord->next = NULL;
			while (ptr->next!=NULL){
				ptr = ptr->next;
			}
			ptr->next = currWord;
			char** typos;
			typos = gentypos(n, word);
			struct misspelled_word* mptr;
			for (int i = 0; i < n; i++){
				struct misspelled_word* currMisspelling;
				if((currMisspelling = (struct misspelled_word*) malloc(sizeof(struct misspelled_word))) == NULL)
				{
				    fprintf(stderr,"ERROR: OUT OF MEMORY.\n");
				    return false;
				}
				addMisspelledWord(currMisspelling,currWord,typos[i]);
				free(typos[i]);
				currMisspelling->next = NULL;
				mptr = m_list;
				while (mptr->next!=NULL){
					mptr = mptr->next;
				}
				mptr->next = currMisspelling;
			}
			free(typos);
			fwrite(word,1,strlen(word),f);
			endWord++;
			while (*endWord!='\0'){
				fputc(*endWord,f);
				endWord++;
			}
			return true;
		} else {
			fwrite(word,1,strlen(word),f);
			endWord++;
			while (*endWord!='\0'){
				fputc(*endWord,f);
				endWord++;
			}
			return true;
		}
		return true;
	}
	//sets inputWord to point to first occurence of word
}

// these functions look fine lol, remember that they store the correct word back into the array that was passed into args
bool foundMisspelledMatch(char* inputWord){
    struct misspelled_word* listPtr = m_list;
    while(listPtr != NULL)
    {
        if(strcasecmp(inputWord, listPtr->word) == 0)
        {
            strcpy(inputWord, listPtr->correct_word->word);
            listPtr->misspelled = true;
            listPtr->correct_word->misspelled_count++;
            return true;
        }
        listPtr = listPtr->next;
    }
    return false;
}

bool foundDictMatch(char* inputWord){
    struct dict_word* listPtr = dict->word_list;
    while(listPtr != NULL)
    {
        if(strcasecmp(inputWord, listPtr->word) == 0)
            return true;
        listPtr = listPtr->next;
    }
    return false;
}

void newDictionary(char* name){
	FILE* d;
	struct dict_word* currWord = dict->word_list;
	d = fopen(name,"w");
	while (currWord->next!=NULL){
		fwrite(currWord->word,1,strlen(currWord->word),d);
		fputc(' ',d);
		for (int i = 0; i < (currWord->num_misspellings);i++){
			fwrite(currWord->misspelled[i]->word,1,strlen(currWord->misspelled[i]->word),d);
			fputc(' ',d);
		}
		fputc('\n',d);
		currWord = currWord->next;
	}
	fwrite(currWord->word,1,strlen(currWord->word),d);
	fputc(' ',d);
	for (int i = 0; i < (currWord->num_misspellings);i++){
		fwrite(currWord->misspelled[i]->word,1,strlen(currWord->misspelled[i]->word),d);
		fputc(' ',d);
	}
	fputc('\n',d);
	fclose(d);
}
