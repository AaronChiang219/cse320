#include "sfish.h"
#include "debug.h"

/*
 * As in previous hws the main function must be in its own file!
 */

int main(int argc, char const *argv[], char* envp[]){
    /* DO NOT MODIFY THIS. If you do you will get a ZERO. */
    rl_catch_signals = 0;
    /* This is disable readline's default signal handlers, since you are going to install your own.*/
    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGTSTP);
    sigprocmask(SIG_BLOCK, &mask, NULL);
    Signal(SIGALRM, alarm_handler);
    Signal(SIGUSR2, user_handler);
    struct sigaction act;
    memset (&act, '\0', sizeof(act));
    act.sa_sigaction = &child_handler;
    act.sa_flags = SA_SIGINFO;
    if(sigaction(SIGCHLD, &act, NULL)<0){
      unix_error("Sigchild handler error");
    }
    char *cmd;
    char *prevwd = getcwd(NULL, 0);
    char *cwd = getcwd(NULL,0);
    dir = (char*) malloc(strlen(cwd)+30);
    strcpy(dir, "<aachiang> : <");
    strcat(dir, cwd);
    strcat(dir, "> $ ");
    while((cmd = readline(dir)) != NULL) {
    	//NEED TO CHANGE THIS TO ACCEPT STRINGS IN QUOTES!!!
    	int argc = 0;
    	char* temp = cmd;
    	while (*temp!='\0'){
    		while (*temp==' ' || *temp == '	'){
    			temp++;
    		}
    		if (*temp!='\0'){
    			argc++;
    			if (*temp=='\"'){
    				temp++;
    				while (*temp!='\"' && *temp!='\0'){
    					temp++;
    					if (*temp=='\\' && *(temp+1)=='\"'){
    						temp+=2;
    					}
    				}
    				if (*temp=='\0'){
    					while(*temp!='\"'){
    						int offset = temp-cmd;
    						char *nextline;
    						nextline = readline("> ");
    						cmd = realloc(cmd, strlen(cmd)+strlen(nextline)+15);
    						strcat(cmd, "\n");
    						strcat(cmd,nextline);
    						free(nextline);
    						temp = cmd+offset;
    						while(*temp!='\"' && *temp!='\0'){
    							temp++;
    							if(*temp=='\\' && *(temp+1)=='\"'){
    								temp+=2;
    							}
    						}
    					}
    				}
    			}
    			while(*temp!=' ' && *temp!='	' && *temp!='\0'){
    				temp++;
    			}
    		}
    	}
    	char **argv;
       	buildargv(cmd, &argv, argc);
       	if (argc>=1){
       		     if (strcmp(argv[0], "exit")==0 && argc==1){
       		     	//free up memory allocated for built *argv[]
       		     	for (int i = 0; i<argc+1; i++){
       		     		free(argv[i]);
       		     	}
       		     	free(argv);
       		     	break;
       		     }
       		     else if (strcmp(argv[0], "pwd")==0 && argc==1){
       		     	if (Fork()==0){
       		     		printf("%s\n",cwd);
       		     		exit(0);
       		     	} else {
       		     		pause();
       		     	}
       		     }
       		     else if (strcmp(argv[0], "help")==0  && argc==1){
       		     	printf("These shell commands are defined internally, type 'help' to see this list \n" \
       		     			"help: Prints this help message \n" \
       		     			"exit: Exits the shell \n" \
       		     			"cd: Changes the current working directory \n" \
       		     			"pwd: Prints the current working directory\n");
       		     }
       		     else if (strcmp(argv[0], "cd")==0){
       		     	if (argc==1){
       		     		if(chdir(getenv("HOME"))==-1){
       		     			printf("error directory not found\n");
       		     		} else {
       		     			free(prevwd);
       		     			prevwd = cwd;
       		     			cwd = getcwd(NULL,0);
       		     			free(dir);
       		     			dir = (char*) malloc(strlen(cwd)+30);
       		     			strcpy(dir, "<aachiang> : <");
       		     			strcat(dir, cwd);
       		     			strcat(dir, "> $ ");
       		     		}
       		     	} else {
       		     		if (argc>2){
       		     			printf("invalid number of args\n");
       		     		} else {
       		     			if (strcmp(argv[1], ".")==0){
       		     				;
       		     			} else if (strcmp(argv[1], "..")==0){
       		     				free(prevwd);
       		     				prevwd = cwd;
       		     				chdir("..");
       		     				cwd = getcwd(NULL,0);
       		     				free(dir);
       		     				dir = (char*) malloc(strlen(cwd)+30);
       		     				strcpy(dir, "<aachiang> : <");
       		     				strcat(dir, cwd);
       		     				strcat(dir, "> $ ");
       		     			} else if (strcmp(argv[1],"-")==0){
       		     				char *temp = cwd;
       		     				cwd = prevwd;
       		     				prevwd = temp;
       		     				free(dir);
       		     				dir = (char*) malloc(strlen(cwd)+30);
       		     				strcpy(dir, "<aachiang> : <");
       		     				strcat(dir, cwd);
       		     				strcat(dir, "> $ ");
       		     			}else if ((chdir(argv[1]))==-1){
       		     				printf("error directory not found\n");
       		     			} else {
       		     				free(prevwd);
       		     				prevwd = cwd;
       		     				cwd = getcwd(NULL,0);
       		     				free(dir);
       		     				dir = (char*) malloc(strlen(cwd)+30);
       		     				strcpy(dir, "<aachiang> : <");
       		     				strcat(dir, cwd);
       		     				strcat(dir, "> $ ");
       		     			}
       		     		}
       		     	}
       		     } else if (strcmp(argv[0],"alarm")==0 && argc == 2) {
       		     	char* temp = argv[1];
                alm = 0;
                while(*temp!='\0' && *temp<='9' && *temp>='0'){
                  alm = alm*10 + (*temp-'0');
                  temp++;
                }
       		     	alarm(alm);
       		     } else {
       		     	//part 3 is to implement file redirection where you search for |,> and < operators
       		     	//reread chapter 10 on I/O and dup2 to reroute input and output
       		     	int fd1 = 0;
       		     	int fd2 = 1;
                int err = 0;
       		     	int last = 0;
                int save = 0;
       		     	int fd[2];
       		     	for (int i = 0; i < argc; i++){
       		     		if (strcmp(argv[i],"|")==0){
       		     			char* temp = argv[i];
       		     			argv[i] = NULL;
       		     			pipe(fd);
       		     			runchild(fd1, fd[1], argv+last, cwd);
       		     			close(fd[1]);
       		     			fd1 = fd[0];
       		     			argv[i] = temp;
       		     			last = i+1;
       		     		}
                  if (strcmp(argv[i],">>")==0){
                    if(argv[i+1]==NULL){
                        printf("Syntax error\n");
                    } else {
                      fd2 = open(argv[i+1],O_WRONLY | O_CREAT | O_APPEND, S_IRWXU | S_IRWXG | S_IRWXO);
                      if(fd2==-1){
                        printf("Error while opening or creating file\n");
                      } else {
                        free(argv[i]);
                        argv[i] = NULL;
                        break;
                      }
                    }
                  }
                  if (strcmp(argv[i],"&>")==0){
                    if(argv[i+1]==NULL){
                        printf("Syntax error\n");
                    } else {
                      fd2 = open(argv[i+1],O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO);
                      if(fd2==-1){
                        printf("Error while opening or creating file\n");
                      } else {
                        err = dup(2);
                        dup2(fd2, 2);
                        free(argv[i]);
                        argv[i] = NULL;
                        break;
                      }
                    }
                  }
       		     		if (strcmp(argv[i],"<")==0){
       		     			if (argv[i+1]==NULL){
       		     				printf("Syntax error\n");
       		     				break;
       		     			} else {
       		     				fd1 = open(argv[i+1],O_RDONLY,0);
       		     				if (fd1==-1){
       		     					printf("Invalid file name\n");
       		     					break;
       		     				} else {
       		     					if (i+3<argc){
       		     						if (strcmp(argv[i+2],">")==0){
       		     							fd2 = open(argv[i+3], O_WRONLY | O_CREAT, S_IRWXU | S_IRWXG | S_IRWXO);
       		     						}
       		     					}
       		     					free(argv[i]);
       		     					argv[i] = NULL;
       		     					break;
       		     				}
       		     			}
       		     		}
       		     	  if (strcmp(argv[i],">")==0){
       		     			if (argv[i+1]==NULL){
       		     				printf("Syntax error\n");
       		     				break;
       		     			} else {
       		     				fd2 = open(argv[i+1],O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO);
       		     				if (fd2==-1){
       		     					printf("Error while creating file\n");
       		     				} else {
       		     					free(argv[i]);
       		     					argv[i] = NULL;
       		     					break;
       		     				}
       		     			}
       		     		}
                  if (*argv[i]<='9' && *argv[i]>='0' && strrchr(argv[i],'>')!=NULL){
                    int desc = 0;
                    char* ptr = argv[i];
                    while (*ptr != '>' && *ptr<='9' && *ptr>='0'){
                      desc = desc*10+(*ptr-'0');
                      ptr++;
                    }
                    int temp = dup(desc);
                    if (argv[i+1]==NULL || temp==-1){
                      printf("Syntax error\n");
                      break;
                    } else {
                      fd2 = open(argv[i+1],O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO);
                      if (fd2==-1){
                        printf("Error while creating file\n");
                      } else {
                        free(argv[i]);
                        argv[i] = NULL;
                        if (desc!=1){
                          dup2(fd2,desc);
                          fd2 = 1;
                          runchild(fd1,fd2,argv+last,cwd);
                          dup2(temp,desc);
                          close(temp);
                          fd2 = -1;
                        }
                        break;
                      }
                    }
                  }
                  if (strcmp(argv[i], "<<")==0){
                    //USE A PIPE TO REDIRECT INPUT TO PROGRAM
                    //WRITE TO WRITE END OF PIPE AND SEND READ END TO PROGRAM
                    if(argv[i+1]==NULL && argc==i+1){
                      printf("Syntax error\n");
                    } else {
                      free(argv[i]);
                      argv[i] = NULL;
                      char* stream = malloc(1);
                      char* bleh;
                      *stream = '\0';
                      while((bleh = readline("> "))!=NULL){
                        if (strstr(bleh,argv[i+1])!=NULL){
                          *strstr(bleh,argv[i+1]) = '\0';
                          stream = realloc(stream, strlen(stream)+strlen(bleh)+5);
                          strcat(stream, bleh);
                          free(bleh);
                          break;
                        } else {
                          stream = realloc(stream, strlen(stream)+strlen(bleh)+5);
                          strcat(stream,bleh);
                          strcat(stream, "\n");
                          free(bleh);
                        }
                      }
                      pipe(fd);
                      save = dup(1);
                      dup2(fd[1],1);
                      write(1, stream, strlen(stream));
                      close(fd[1]);
                      dup2(save,1);
                      close(save);
                      fd1 = fd[0];
                      free(stream);
                  }
       		       }
              }
       		     	if(fd1>=0 && fd2>=0){
       		     		if (last!=0){
       		     			fd2 = 1;
       		     		}
       		     		runchild(fd1,fd2,argv+last,cwd);
       		     		if (last!=0 || save!=0){
    						    close(fd1);
       		     		}
                  if (err!=0){
                    dup2(err, 2);
                    close(err);
                  }
       		     	}
       		 	}
       	}
        /* All your debug print statements should use the macros found in debu.h */
        /* Use the `make debug` target in the makefile to run with these enabled. */
        //info("Length of command entered: %ld\n", strlen(cmd));
        /* You WILL lose points if your shell prints out garbage values. */
        for (int i = 0; i<argc+1; i++){
        	free(argv[i]);
        }
        free(argv);
        free(cmd);
    }
    free(cwd);
    free(dir);
    /* Don't forget to free allocated memory, and close file descriptors. */
    free(cmd);
    free(prevwd);

    return EXIT_SUCCESS;
}
