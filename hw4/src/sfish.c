#include "sfish.h"

void unix_error(char *msg){
	fprintf(stderr, "%s: %s\n", msg, strerror(errno));
	exit(0);
}

void alarm_handler(int sig){
	printf("\nYour %d second timer has finished!\n%s", alm,dir);
	fflush(stdout);
}

void user_handler(int sig){
	printf("Well that was easy.\n");
	fflush(stdout);
}

void child_handler(int sig, siginfo_t *siginfo, void *context){
	int pid;
	int x = (int)(((1000* (double)(siginfo->si_stime + (double)siginfo->si_utime)))/(double)sysconf(_SC_CLK_TCK));
	pid = waitpid(-1, NULL, WNOHANG | WUNTRACED);
		printf("Child with PID %d has died. It spent %d milliseconds utilizing the CPU\n",pid,x);
}
handler_t *Signal(int signum, handler_t *handler)
{
    struct sigaction action, old_action;

    action.sa_handler = handler;
    sigemptyset(&action.sa_mask); /* Block sigs of type being handled */
    action.sa_flags = SA_RESTART; /* Restart syscalls if possible */

    if (sigaction(signum, &action, &old_action) < 0)
	unix_error("Signal error");
    return (old_action.sa_handler);
}

pid_t Fork(){
	pid_t pid;

	if((pid = fork())<0){
		unix_error("fork error");
	}
	return pid;
}

void buildargv(char *args, char*** argv, int count){
	*argv = (char**) malloc((count+1)*(sizeof(char*)));
	if (count==0){
		*(*argv) = NULL;
		return;
	}
	char *copy = args;
	char* string = malloc(strlen(copy)+1);
	int a = 0;
	int b = 0;
	while(*args!='\0'){
		switch(*args){
			case ' ':
			case '\t':
			case '\0':
			if (a==0){
				args++;
			} else {
				string[a] = '\0';
				a = 0;
				*(*argv+b) = string;
				b++;
				string = malloc(strlen(copy)+1);
			}
			break;
			case '\"':
			args++;
			while(*args!='\"' && *args!='\0'){
				if (*args=='\\' && *(args+1)=='\"'){
					args+=2;
					string[a] = '\"';
					a++;
				} else {
					string[a] = *args;
					a++;
					args++;
				}
			}
			if (*args!='\0'){
				args++;
			}
			break;
			default:
			while(*args!=' ' && *args!='\t' && *args!='\0'){
				string[a] = *args;
				a++;
				args++;
			}
		}
	}
	string[a] = '\0';
	a = 0;
	*(*argv+b) = string;
	b++;
 	*(*argv+b) = NULL;

}

void runchild(int in, int out, char** argv, char* cwd){
	//made runchild its own function so that I can use redirection
 	//forgot to search path for a /, if it exist then attempt to run file
 	int truth = 0;
 	for(char* temp = argv[0]; *temp!='\0'; temp++){
 		if (*temp=='/'){
 			truth = 1;
 		}
 	}
 	if (truth) {
 		char* file = (char*) malloc(strlen(cwd) + strlen(argv[0])+4);
 		strcpy(file, cwd);
 		if(*argv[0]=='/'){
 			strcpy(file,argv[0]);
 		} else if (*argv[0]=='.'){
 			strcat(file,argv[0]+1);
 		} else {
 			strcat(file, "/");
 			strcat(file, argv[0]);
 		}
 		if(Fork()==0){
 			if(in!=0){
 				dup2(in, 0);
 				close(in);
 			}
 			if(out!=1){
 				dup2(out,1);
 				close(out);
 			}
 			if (execv(file, argv)==-1){
 				printf("Error: Executable not found\n");
 				free(file);
 				exit(0);
 			}
 		} else {
 			pause();
 			free(file);
 		}
 	} else {
 		//search path file
 		if (Fork()==0){
 			if(in!=0){
 				dup2(in, 0);
 				close(in);
 			}
 			if(out!=1){
 				dup2(out,1);
 				close(out);
 			}
 			char *path = strdup(getenv("PATH"));
 			char *temp = strtok(path, ":");
 			while(temp!=NULL){
 				char* file = (char*) malloc(strlen(temp)+strlen(argv[0])+4);
 				strcpy(file, temp);
 				strcat(file,"/");
 				strcat(file,argv[0]);
 				execv(file, argv);
 				free(file);
 				temp = strtok(NULL,":");
 			}
 			printf("Error: Command not found\n");
 			exit(0);
 		} else {
 			pause();
 		}
	 }
}

