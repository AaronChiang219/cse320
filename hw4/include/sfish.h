#ifndef SFISH_H
#define SFISH_H
#include <readline/readline.h>
#include <readline/history.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>

pid_t Fork();
void unix_error(char *msg);
void buildargv(char *args, char*** argv, int count);
void runchild(int in, int out, char** argv, char* cwd);
void alarm_handler(int sig);
void user_handler(int sig);
void child_handler(int sig, siginfo_t *siginfo, void *context);
typedef void handler_t(int);
handler_t *Signal(int signum, handler_t *handler);
int alm;
char *dir;
#endif
