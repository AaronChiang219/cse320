#ifndef SERVER_H
#define SERVER_H

#include <pthread.h>

#include "debug.h"
#include "arraylist.h"
#include "foreach.h"
#include "const.h"
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <signal.h>

typedef struct{
	int fd;
	char* name;
}user;


int toInteger(char *string);
void* login_thread();
void* comms_thread();
void* broadcast_thread();
typedef void handler_t(int);
handler_t *Signal(int signum, handler_t *handler);
void close_handler(int sig);
void freeName(void* ptr);
arraylist_t *login;
arraylist_t *users;
fd_set ready_set;
fd_set read_set;
sem_t setmutex;
int max;
#endif
