#include <criterion/criterion.h>
#include <criterion/logging.h>
#include <stdio.h>
#include <stdbool.h>
#include "arraylist.h"
#include "foreach.h"

/******************************************
 *                  ITEMS                 *
 ******************************************/
arraylist_t *global_list;

typedef struct {
    char* name;
    int32_t id;
    double gpa;
}student_t;

typedef struct{
    int i;
    float f;
    long double ld;
    char c1:4;
    char c2:4;
    short s;
    void *some_data;
}test_item_t;

/******************************************
 *              HELPER FUNCS              *
 ******************************************/
void test_item_t_free_func(void *argptr){
    test_item_t* ptr = (test_item_t*) argptr;
    if(!ptr)
        free(ptr->some_data);
    else
        cr_log_warn("%s\n", "Pointer was NULL");
}

void setup(void) {
    cr_log_warn("Setting up test");
    global_list = new_al(sizeof(int));
}

void teardown(void) {
    cr_log_error("Tearing down");
    delete_al(global_list, NULL);
}

/******************************************
 *                  TESTS                 *
 ******************************************/
Test(al_suite, 0_creation, .timeout=2){
    arraylist_t *locallist = new_al(sizeof(int));

    cr_assert_not_null(locallist, "List returned was NULL");
}

Test(al_suite, 1_deletion, .timeout=2){
    arraylist_t *locallist = new_al(sizeof(int));

    cr_assert_not_null(locallist, "List returned was NULL");

    delete_al(locallist, test_item_t_free_func);

    cr_assert(true, "Delete completed without crashing");
}

Test(al_suite, 2_insertion, .timeout=2, .init=setup, .fini=teardown){
    //arraylist_t *list = new_al(sizeof(int));
    for (int i = 0; i < 100; i++){
        int* item = malloc(sizeof(int));
        *item = i;
        insert_al(global_list, item);
        free(item);
    }
    for (int i = 0; i < 100; i++){
        int* test = get_index_al(global_list,i);
        printf("%d\n",*test);
    }
    for (int i = 0; i < 99; i++){
        remove_index_al(global_list, 0);
    }
    for (int i = 0; i < 1; i++){
        int* test = get_index_al(global_list, i);
        printf("%d\n",*test);
    }
    printf("%zu",global_list->capacity);
    cr_assert(true, "I win");
}

void* func(void* ptr) {
    int index = *((int*) ptr);
    if(index % 2 == 0){
        //do stuff
        int* xx = malloc(sizeof(int));
        *xx = index;
        remove_index_al(global_list, 0);
    }
    else{
        int* xx = malloc(sizeof(int));
        *xx = index;
        insert_al(global_list, xx);
    }
    return NULL;
}

Test(al_suite, stuff, .init=setup, .fini=teardown) {
    printf("stuff test\n");
    for(int i=0; i<50; ++i) {
        int *ptr = (int*) malloc(sizeof(int));
        *ptr = i;
        insert_al(global_list, ptr);
    }
    pthread_t ids[10];
    for(int i=0; i<10; ++i) {
        int* ptr = malloc(sizeof(int));
        *ptr = i;
        pthread_create(&ids[i], NULL, func, ptr);
    }
    for(int i=0; i<10; ++i) {
        pthread_join(ids[i], NULL);
    }
    cr_assert_eq(global_list->length, 50);
    cr_assert_eq(global_list->capacity, 64);
}

void *readlist(){
    int sum = 0;
    for (int i = 0; i < 10; i++){
        foreach(int, data, global_list){
            remove_index_al(global_list, 0);
            sum += *data;
        }
    }
    printf("%d\n",sum);
    return NULL;
}
void *addtolist(){
    int *insert = malloc(sizeof(int));
    *insert = pthread_self();
    for (int i = 0; i < 20; i++){
        insert_al(global_list, insert);
    }
    return NULL;
}


Test(al_suite, foreach, .init=setup, .fini=teardown){
    printf("foreach test\n");
    for (int i=0; i<500; i++){
        int *p = malloc(sizeof(int));
        *p = i;
        insert_al(global_list, p);
    }
    pthread_t threads[200];
    for(int i = 0; i < 200; i++){
        if (i%2==1){
            pthread_create(&threads[i],NULL,readlist, NULL);
        } else {
            pthread_create(&threads[i],NULL,addtolist,NULL);
        }

    }
    for (int i = 0; i < 200; i++){
        pthread_join(threads[i],NULL);
    }
    foreach(int, data, global_list){
        printf("%d\n",*data);
    }
    printf("%zu", global_list->length);
    //do some threaded tests for foreach
}

int dostuff(void* ptr){
    int *integer = ptr;
    *integer += 1;
    return 0;
}

Test(al_suite, apply, .init=setup, .fini=teardown){
    for(int i = 0; i<400; i++){
        int*p = malloc(sizeof(int));
        *p = i;
        insert_al(global_list,p);
        free(p);
    }
    apply(global_list, dostuff);
    foreach(int, data, global_list){
        printf("%d\n", *data);
    }
}


