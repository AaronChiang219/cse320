#include "server.h"
#include <errno.h>
int main(int argc, char *argv[]){
	sigset_t mask;
	sigemptyset(&mask);
	sigaddset(&mask, SIGPIPE);
	sigprocmask(SIG_BLOCK, &mask, NULL);
	Signal(SIGINT, close_handler);

	int socket_server, socket_client;
	struct sockaddr_in server, client;
	Sem_init(&(setmutex), 0, 1);
	FD_ZERO(&read_set);
	FD_SET(STDIN_FILENO, &read_set);
	max = 1;
	socket_server = socket(AF_INET, SOCK_STREAM, 0);
	if(socket_server == -1){
		printf("Socket creation error");
		return 1;
	}

	if (argc!=2){
		return -1;
	}
	int threadcount = toInteger(argv[1]);
	if (threadcount==-1){
		printf("Invalid number of threads specified");
		return -1;
	}
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = htons(PORT);
	if (bind(socket_server,(struct sockaddr*) &server, sizeof(server))<0){
		return -1;
	}
	if (listen(socket_server, 1024)<0){
		return -1;
	}
	//This will hold all the sockets for the login threads to place in the user arraylist
	login = new_al(sizeof(struct sockaddr));
	//User arraylist to hold the current users connected to server
	users = new_al(sizeof(user));
	int addrlen = sizeof(struct sockaddr_in);
	pthread_t threadid;
	while (threadcount!=0){
		pthread_create(&threadid, NULL, login_thread, NULL);
		threadcount--;
	}

	pthread_create(&threadid, NULL, comms_thread, NULL);
	while ((socket_client = accept(socket_server, (struct sockaddr*) &client,(socklen_t*) &addrlen))){
		//accept connections by inserting them into the arraylist and letting the login threads handle them
		if (socket_client<0){
			;
		} else {
			insert_al(login, &socket_client);
		}
	}
}

void* login_thread(){
	pthread_detach(pthread_self());
	int* socket = NULL;
	char message[2000];
	char message2[100];
	while(1){
		socket = remove_index_al(login, 0);
		if (socket==NULL){
			;
		} else {
			int end = recv(*socket, message, 2000, 0);
			message[end] = '\0';
			recv(*socket, message2, 100, 0);
			//write(*socket, message, 50);
			if (strncmp("ALOLA! \r\n", message, 9)==0 && strncmp("\r\n", message2, 2)==0){
				write(*socket, "!ALOLA \r\n\r\n", 11);
				end = recv(*socket, message, 2000, 0);
				message[end] = '\0';
				recv(*socket, message2, 100, 0);
				if (strncmp(message, "IAM ", 4)==0 && strncmp("\r\n", message2, 2)==0){
					char *name = strndup(message+4, strlen(message)-6);
					bool taken = false;
					foreach(user, value, users){
						if (value->name!=NULL){
							if (strcmp(name, value->name)==0){
								write(*socket, "ETAKEN ", 7);
								write(*socket, name, strlen(name));
								write(*socket, "\r\n\r\n", 4);
								taken = true;
								free(value);
								value = NULL;
							} else {
								free(value);
								value = NULL;
							}
						} else {
							free(value);
							value = NULL;
						}

					}
					if (taken == false){
						write(*socket, "MAI ",4);
						write(*socket, message+4, strlen(message)-4);
						write(*socket, "\r\n", 2);
						user *insert = malloc(sizeof(user));
						insert->fd = *socket;
						insert->name = name;
						insert_al(users, insert);
						free(insert);
						P(&setmutex);
						if (*socket>=max){
							max = *socket+1;
						}
						FD_SET(*socket, &read_set);
						V(&setmutex);
						free(socket);

					} else {
						close(*socket);
						free(socket);
					}
				}

			} else {
				close(*socket);
				free(socket);
				socket = NULL;
			}
		}
	}
}
void* comms_thread(){
	pthread_detach(pthread_self());
	struct timeval timeout;
	timeout.tv_sec = 1;
	timeout.tv_usec = 1;
 	pthread_t blah = 0;
 	int i = -1;
	while(1){
		timeout.tv_sec = 1;
		timeout.tv_usec = 1;
		ready_set = read_set;
		select(max, &ready_set, NULL, NULL, &timeout);
		foreach(user, value, users){
			if (FD_ISSET(value->fd, &ready_set)){
				recv(value->fd, &i, 1, MSG_PEEK);
				if (i != -1){
					pthread_create(&blah, NULL, broadcast_thread, value);
					value = NULL;
					i = -1;
				} else {
					free(value->name);
					value->name = NULL;
					FD_CLR(value->fd, &read_set);
					i = -1;
				}
			} else {
				free(value);
				value = NULL;
			}
		}
	}
}

void* broadcast_thread(void* ptr){
	pthread_detach(pthread_self());
	user *client = ptr;
	int socket = client->fd;
	char* name = client->name;
	char message[2000];
	char message2[200];
	int end = recv(socket, message, 2000, 0);
	message[end] = '\0';
	recv(socket, message2, 200, 0);
	if (strncmp(message, "MSG ", 4)==0 && strncmp(message2,"\r\n", 2)==0){
		char *m = malloc(strlen(message)+strlen(name));
		strncpy(m, "MSG ",4);
		strncpy(m+4, name, strlen(name));
		strncpy(m+4+strlen(name), ": ", 2);
		strncpy(m+6+strlen(name), message+4, strlen(message)-4);
		foreach(user, value, users){
			if (value->fd!=socket){
				write(value->fd, m, strlen(m)-1);
				write(value->fd, "\r\n\r\n", 4);
				free (value);
				value = NULL;
			} else {
				write(socket, "GSM ", 4);
				write(socket, message+4, strlen(message)-4);
				write(socket, "\r\n", 2);
				free(value);
				value = NULL;
			}
		}
		free(m);
	} else if (strncmp(message, "WHO", 3)==0 && strncmp(message2, "\r\n", 2)==0){
		write(socket, "OHW ", 4);
		foreach(user, value, users){
			if (value->name != NULL){
				write(socket, value->name, strlen(value->name));
				write(socket, "\r\n", 2);
			}
			free(value);
			value = NULL;
		}
		write(socket, "\r\n", 2);
	}
	free(ptr);
	return NULL;
}

int toInteger(char* string){
	int ret = 0;
	while(*string!='\0'){
		if (*string<='9' && *string>='0'){
			ret = ret*10+(*string-'0');
			string++;
		} else {
			return -1;
		}
	}
	return ret;
}

handler_t *Signal(int signum, handler_t *handler)
{
    struct sigaction action, old_action;

    action.sa_handler = handler;
    sigemptyset(&action.sa_mask); /* Block sigs of type being handled */
    action.sa_flags = SA_RESTART; /* Restart syscalls if possible */

    if (sigaction(signum, &action, &old_action) < 0)
	printf("signal error");
    return (old_action.sa_handler);
}

void close_handler(int sig){
	delete_al(users, freeName);
	delete_al(login, NULL);
	free(login);
	free(users);
	exit(0);
}

void freeName(void *ptr){
	user* person = ptr;
	if(person->name!=NULL){
		free(person->name);
	}
}

