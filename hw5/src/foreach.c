#include "debug.h"
#include "arraylist.h"
#include "foreach.h"
#include <errno.h>

static pthread_once_t once = PTHREAD_ONCE_INIT;

void *foreach_init(void *self){
    arraylist_t *arraylist = self;
    pthread_once(&once, init_keys);
    if (errno == EAGAIN || errno == ENOMEM){
        return NULL;
    } else {
        int *i = malloc(sizeof(int));
        *i = 0;
        pthread_setspecific(key, i);
        pthread_setspecific(key2, self);
        if (errno == ENOMEM || errno == EINVAL){
            return NULL;
        }
    }
    int *index = pthread_getspecific(key);
    P(&(arraylist->foreachmutex));
    arraylist->foreachcount += 1;
    if (arraylist->foreachcount==1){
        pthread_mutex_lock(&(arraylist->foreach));
    }
    V(&(arraylist->foreachmutex));
    return get_index_al(arraylist, *index);
}

void *foreach_next(void *self, void* data){
    int *index = pthread_getspecific(key);
    if (data!=NULL){
        set_item_al(self, *index, data);
        free(data);
    }
    arraylist_t *list = self;
    P(&(list->write));
    *index += 1;
    pthread_setspecific(key, index);
    if (*index >= list->length){
        V(&(list->write));
        foreach_break_f();
        return NULL;
    }
    V(&(list->write));
    void* ret = get_index_al(self, *index);
    if (ret==NULL){
        foreach_break_f();
        return NULL;
    } else {
        return ret;
    }
}

size_t foreach_index(){
    size_t ret = 0;
    int *index = pthread_getspecific(key);
    if (errno == ENOMEM || errno == EINVAL){
        return UINT_MAX;
    }
    ret = *index;
    return ret;
}

bool foreach_break_f(){
    arraylist_t *list = pthread_getspecific(key2);
    int *index = pthread_getspecific(key);
    free(index);
    P(&(list->foreachmutex));
    list->foreachcount -= 1;
    if (list->foreachcount==0){
        pthread_mutex_unlock(&(list->foreach));
    }
    V(&(list->foreachmutex));
    return true;
}

int32_t apply(arraylist_t *items, int32_t (*application)(void*)){
    for (void *value = foreach_init(items);value;value = foreach_next(items, value)){
        if((*application)(value)==-1){
            free(value);
            value = NULL;
        }
    }
    return 0;
}

void init_keys(){
    pthread_key_create(&key, NULL);
    pthread_key_create(&key2, NULL);
}
