#include "arraylist.h"
#include <errno.h>

/**
 * @visibility HIDDEN FROM USER
 * @return     true on success, false on failure
 */

static bool resize_al(arraylist_t* self){
	//use semaphore to make sure no one else is resizing or modifying the array currently
	//when growing, it will double in size when length==capacity
	//when shrinking it will halve when length==(1/2)capacity-1
	//NEVER SHRINK BELOW INIT_SZ, so CHECK THIS FIRST
	//resize may cause changes to pointer so lock array operations and return valid pointer
    int length = self->length;
    int capacity = self->capacity;
    int item_size = self->item_size;
    if (length==capacity){
    	int size = (capacity)*(item_size)*2;
    	self->base = realloc(self->base, size);
    	self->capacity = capacity*2;
    	if (self->base == NULL){
    		return false;
    	} else {
    		return true;
    	}
    } else if (length==(capacity/2)-1) {
    	if ((capacity/2)>=INIT_SZ){
    		int size = (capacity/2)*item_size;
    		self->base = realloc(self->base, size);
    		self->capacity = capacity/2;
    		if (self->base == NULL){
    			return false;
    		} else {
    			return true;
    		}
    	}
    }
    return true;
}

arraylist_t *new_al(size_t item_size){
	//malloc an instance of arraylist_t and fill out the corresponding variables
	//initialize the arraylist pointed to by arraylist_t and also create a seperate
	//arraylist for semaphores that correspond to the current arraylist
    arraylist_t *ret = (arraylist_t*)malloc(sizeof(arraylist_t));
    ret->capacity = INIT_SZ;
    ret->length = 0;
    ret->item_size = item_size;
    ret->foreachcount = 0;
    ret->base = calloc(INIT_SZ, item_size);
    Sem_init(&(ret->mutex), 0, 1);
    Sem_init(&(ret->write), 0, 1);
    Sem_init(&(ret->foreachmutex),0,1);
   	pthread_mutex_init(&(ret->foreach), NULL);
    ret->readcount = 0;


    return ret;
}

size_t insert_al(arraylist_t *self, void* data){
	//check if any other threads are currently inserting and if not insert at the end
	//should not matter if other threads are modifying data since you are adding a new element
	//check length vs capacity and call resize al if needed, otherwise insert element where length is
	int ret = 0;
	P(&(self->write));
	if (self->capacity == 0){
		errno = ENOENT;
		V(&(self->write));
		return  UINT_MAX;
	}
	void* base = self->base;
	int offset = (self->length)*(self->item_size);
	memcpy((char*)base+offset, data, self->item_size);
	self->length += 1;
	if (resize_al(self)==false){
		ret = UINT_MAX;
		errno = ENOMEM;
	} else {
		ret = self->length-1;
	}
    V(&(self->write));
    return ret;
}

size_t get_data_al(arraylist_t *self, void *data){
	//return the index of the item equal to data, can run only when reading operations are allowed
	//check semaphore to make sure writing isn't done, or else there may be concurrency issues
	//if nothing is writing then lock so reading only
	//if data is NULL return first item, if data not found then return error
	//when you do this, lock the data as if it were being written to to stop writes to this index until accessed
	//when accessing data then, check to see if it was locked and then free the data
	int position = -1;
	P(&(self->mutex));
	if (self->capacity == 0 || self->length == 0){
		errno = ENOENT;
		self->readcount-= 1;
		V(&(self->mutex));
		return UINT_MAX;
	}
	V(&(self->mutex));
	if (self->capacity == 0){
		errno = ENOENT;
		return UINT_MAX;
	}
	self->readcount += 1;
	if (self->readcount==1){
		P(&(self->write));
	}
	if (data!=NULL){
		for (int i = 0; i<self->length; i++){
			int offset = i*(self->item_size);
			if (memcmp((char*)(self->base)+offset,data, self->item_size)==0){
				position = i;
				break;
			}
		}
	} else {
		position = 0;
	}
	P(&(self->mutex));
	self->readcount -= 1;
	if (self->readcount == 0){
		V(&(self->write));
	}
	V(&(self->mutex));
	if (position==-1){
		errno = ENOMSG;
		return UINT_MAX;
	} else {
		return position;
	}
}

void *get_index_al(arraylist_t *self, size_t index){
	//return a pointer to the COPY of an item at self[index]
	//check semaphores to make sure only reading is allowed
	//if the index exceeds the number of length, then copy the last item in the arraylist
    void *ret = NULL;
    P(&(self->mutex));
    self->readcount+= 1;
    if (self->readcount==1){
    	P(&(self->write));
    }
    if (self->capacity == 0 || self->length == 0){
    	errno = ENOENT;
    	self->readcount-= 1;
    	V(&(self->write));
    	V(&(self->mutex));
    	return NULL;
    }
    V(&(self->mutex));
    void* base = self->base;
    int item_size = self->item_size;
    if (index >= self->length){
    	ret = malloc(item_size);
    	if (ret!=NULL)
    	memcpy(ret, (char*)base+((self->length)-1)*item_size, item_size);
    } else {
    	ret = malloc(self->item_size);
    	if (ret!=NULL)
    	memcpy(ret, (char*)base+(index*item_size), item_size);
    }
    if (ret==NULL){
    	errno = ENOMEM;
    }
    P(&(self->mutex));
    self->readcount -= 1;
    if(self->readcount == 0){
    	V(&(self->write));
    }
    V(&(self->mutex));
    return ret;
}

bool remove_data_al(arraylist_t *self, void *data){
	//remove the item that matches data, make sure nothing is writing or reading at this time
    size_t index = 0;
    index = get_data_al(self,data);
    if (index==UINT_MAX){
    	return false;
    }
    P(&(self->write));
    if(pthread_mutex_trylock(&(self->foreach))!=0){
    	errno = ENOENT;
    	V(&(self->write));
    	return false;
    } else {
    	pthread_mutex_unlock(&(self->foreach));
    }
    if (self->capacity == 0){
    	errno = ENOENT;
    	V(&(self->write));
    	return false;
    }
    void* base = (char*)self->base + (index*self->item_size);
    int cpysize = (self->length-index-1)*(self->item_size);
    memmove(base, (char*)base+self->item_size, cpysize);
    self->length -= 1;
    resize_al(self);
    V(&(self->write));

    return true;
}

void *remove_index_al(arraylist_t *self, size_t index){
	//make a copy and remove the item at index from the list
	//if index is greater than the length then copy and remove the last item in the arraylist
	//if arraylist is length==(1/2)capacity-1 it should resize al
    void *ret = NULL;
    P(&(self->write));
    if(pthread_mutex_trylock(&(self->foreach))!=0){
    	errno = ENOENT;
    	V(&(self->write));
    	return ret;
    } else {
    	pthread_mutex_unlock(&(self->foreach));
    }
    if (self->capacity == 0 || self->length == 0){
    	errno = ENOENT;
    	V(&(self->write));
    	return NULL;
    }
    index = (index>=self->length)?self->length-1:index;
    void* base = (char*)self->base + (index*self->item_size);
    int cpysize = (self->length-index-1)*(self->item_size);
    ret = malloc(self->item_size);
    memcpy(ret, base, self->item_size);
    memmove(base, (char*)base+self->item_size, cpysize);
    self->length -= 1;
    resize_al(self);
    V(&(self->write));

    return ret;
}

bool set_item_al(arraylist_t *self, size_t index, void *data){
	P(&(self->write));
	if (self->capacity == 0 || self->length == 0){
		errno = ENOENT;
		V(&(self->write));
		return false;
	}
	void* base = (char*)self->base + (index*self->item_size);
	memcpy(base, data, self->item_size);
	V(&(self->write));
	return true;
}
void delete_al(arraylist_t *self, void (*free_item_func)(void*)){
	//free the arraylist_t and the semaphore and base array;
	P(&(self->write));
	if(pthread_mutex_trylock(&(self->foreach))!=0){
		errno = ENOENT;
		V(&(self->write));
		return;
	} else {
		pthread_mutex_unlock(&(self->foreach));
	}
	if (self->capacity == 0){
		errno = ENOENT;
		V(&(self->write));
		return;
	}
	if (free_item_func==NULL){
		free(self->base);
	} else {
		void* base = self->base;
		for (int i = 0; i < (self->length); i++){
			void *pointer = (char*)base+(i*(self->item_size));
			(*free_item_func)(pointer);
		}
		free(base);
	}
	self->length = 0;
	self->item_size = 0;
	self->capacity = 0;
	V(&(self->write));
    return;
}

void P(sem_t *sem){
	if (sem_wait(sem)<0)
		unix_error("P error");
}

void V(sem_t *sem){
	if (sem_post(sem) < 0)
		unix_error("V error");
}

void Sem_init(sem_t *sem, int pshared, unsigned int value){
	if (sem_init(sem, pshared, value)<0)
		unix_error("Sem_init error");
}

void unix_error(char *msg){
	fprintf(stderr, "%s: %d\n", msg, errno);
	exit(0);
}
